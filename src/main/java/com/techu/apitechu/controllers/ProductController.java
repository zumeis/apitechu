package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    //  /apitechu/v1/products
    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProduct() {
        System.out.println("getProducts");
        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();
        {
            for (ProductModel product : ApitechuApplication.productModels) {
                if (product.getId().equals(id)) {
                    result = product;
                }
            }
        }
        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("la id del nuevo producto es " +newProduct.getId());
        System.out.println("la descripción del nuevo producto es " +newProduct.getDesc());
        System.out.println("el precio del nuevo producto es " +newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.print("la id en parámetro de la url es " +id);
        System.out.print("la id del producto a actualizar " + product.getId());
        System.out.print("la Descripción del producto a actualizar " + product.getDesc());
        System.out.print("el Precio del producto a actualizar " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;

    }
    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es" + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                foundProduct = true;
                result = product;
            }
        }
        if (foundProduct == true) {
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;

    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("patchProduct");
        System.out.print("la id en parámetro de la url es " +id);
        System.out.print("la id del producto a actualizar " + product.getId());
        System.out.print("la Descripción del producto a actualizar " + product.getDesc());
        System.out.print("el Precio del producto a actualizar " + product.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                result = productInList;

                if (product.getDesc() != null) {
                    productInList.setDesc(product.getDesc());
                    System.out.println(" Se cambia la descripción del producto a " + product.getDesc());
                }
                if (product.getPrice() > 0) {
                    productInList.setPrice(product.getPrice());
                    System.out.println(" Se cambia el precio del producto a " + product.getPrice());
                }

            }
        }
        return product;

    }

}
